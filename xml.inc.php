<?php
/*

MultiSafepay API example for PHP
http://www.multisafepay.com

Copyright (C) 2008 MultiSafepay.com

*/

function xml_escape($str)
{
	return htmlspecialchars($str, ENT_COMPAT, 'UTF-8');
}

function xml_unescape($str)
{
	return strtr($str, array_flip(get_html_translation_table(HTML_SPECIALCHARS, ENT_COMPAT)));
}

function xml_array_to_xml($arr, $name)
{
	$data = "<{$name}>\n";

	foreach ($arr as $key => $value) {
		$value = xml_escape($value);
		$data .= "<{$key}>{$value}</{$key}>\n";
	}

	$data .= "</{$name}>\n";

	return $data;
}
/*
##############################################
# release With Curl
#
##############################################
*/
function xml_post_curl($url, $request_data, &$error_message, $verify_peer = false)
{
	$parsed_url = parse_url($url);

	if (empty($parsed_url['port'])) {
		$parsed_url['port'] = strtolower($parsed_url['scheme']) == 'https' ? 443 : 80;
	}

	$real_url = $parsed_url['scheme'] . "://" . $parsed_url['host'] . ":" . $parsed_url['port'] . "/";

	// generate request
	$header  = "POST " . $parsed_url['path'] ." HTTP/1.1\r\n";
	$header .= "Host: " . $parsed_url['host'] . "\r\n";
	$header .= "Content-Type: text/xml\r\n";
	$header .= "Content-Length: " . strlen($request_data) . "\r\n";
	$header .= "Connection: close\r\n";
	$header .= "\r\n";
	$request = $header . $request_data;

	// issue request
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $verify_peer);
	curl_setopt($ch, CURLOPT_URL,            $real_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT,        30);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  $request);

	$reply_data = curl_exec($ch);

	// check response
	if (curl_errno($ch)) {
		$error_message = curl_error($ch);
		return false;
	}

	$reply_info = curl_getinfo($ch);
	curl_close($ch);

	if ($reply_info['http_code'] != 200) {
		$error_message = 'HTTP code is ' . $reply_info['http_code'] . ', expected 200';
		return false;
	}

	if (strstr($reply_info['content_type'], "/xml") === false) {
		$error_message = 'Content type is ' . $reply_info['content_type'] . ', expected */xml';
		return false;
	}

	return $reply_data;
}
/*
##############################################
# release NO Curl
#
##############################################
*/
function xml_post($url, $request_data, &$error_message, $verify_peer = false)
{
	$parsed_url = parse_url($url);

	if (empty($parsed_url['port'])) {
		$parsed_url['port'] = strtolower($parsed_url['scheme']) == 'https' ? 443 : 80;
	}

	$real_url = $parsed_url['scheme'] . "://" . $parsed_url['host'] . ":" . $parsed_url['port'] . "/";

	// generate request
	$header  = "POST " . $parsed_url['path'] ." HTTP/1.1\r\n";
	$header .= "Host: " . $parsed_url['host'] . "\r\n";
	$header .= "Content-Type: text/xml\r\n";
	$header .= "Content-Length: " . strlen($request_data) . "\r\n";
	$header .= "Connection: close\r\n";
	$header .= "\r\n";
	$request      .= $header . $request_data;
	$reply_data    = "";
	$errno  = 0;
	$errstr = "";
	$fp = fsockopen(($parsed_url['scheme'] == "https" ? "ssl://" : "") . $parsed_url['host'], $parsed_url['port'], $errno, $errstr, 30);

	if ($fp) {
		if (function_exists("stream_context_set_params")) {
			stream_context_set_params($fp, array(
				'ssl' => array(
					'verify_peer'       => $verify_peer,
					'allow_self_signed' => $verify_peer
				)
			));
		}

		fwrite($fp, $request);
		fflush($fp);

		while (!feof($fp)) {
			$reply_data .= fread($fp, 1024);
		}

		fclose($fp);
	}
	if ($errno) {
		$this->_error_redirect(2000);
	}
	$header_size  = strpos($reply_data, "\r\n\r\n");
	$header_data  = substr($reply_data, 0, $header_size);
	$header       = explode("\r\n", $header_data);
	$status_line  = explode(" ", $header[0]);
	$content_type = "application/octet-stream";

	foreach ($header as $header_line) {
		$header_parts = explode(":", $header_line);

		if (strtolower($header_parts[0]) == "content-type") {
			$content_type = trim($header_parts[1]);
			break;
		}
	}

	$reply_info = array(
		'http_code'    => (int) $status_line[1],
		'content_type' => $content_type,
		'header_size'  => $header_size + 4
	);

	if ($reply_info['http_code'] != 200) {
		$error_message = 'HTTP code is ' . $reply_info['http_code'] . ', expected 200';
		return false;
	}

	if (strstr($reply_info['content_type'], "/xml") === false) {
		$error_message = 'Content type is ' . $reply_info['content_type'] . ', expected */xml';
		return false;
	}

	// split header and body
	$reply_header = substr($reply_data, 0, $reply_info['header_size'] - 4);
	$reply_xml    = substr($reply_data, $reply_info['header_size']);

	return $reply_xml;
}


?>
