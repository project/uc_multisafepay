$(document).ready(function(){
	//Handle the ids
	$('#payments td.orderid').each(
		function( intIndex ) {
			order = this.innerHTML;
			querystring = 'order=' + order;
			
			$.ajax({
				type: 'GET',
				url: '/multisafepay/ajax/getstatus',
				data: querystring,
				success: function (msg) {
					uc_multisafepay_statussuccess(msg);
				}
			})
		}
	);
});

function uc_multisafepay_statussuccess( msg ) {
	msg = msg.split('#');
	
	$('#mspstatus_' + msg[0]).text('')
	$('#mspstatus_' + msg[0]).text(msg[1]);
}

function uc_multisafepay_changestatus( obj, drupaltoken ) {
	id = obj.id;
	status = obj.value;
	
	id = id.split('_');
	id = id[1];
	
	querystring = 'order=' + id + '&status=' + status + '&token=' + drupaltoken;
	
	$.ajax({
		type: 'GET',
		url: '/multisafepay/ajax/updatestatus',
		data: querystring,
		success: function (msg) {
			uc_multisafepay_statusupdatesuccess(msg);
		}
	})
}

function uc_multisafepay_statusupdatesuccess( msg ) {
	if (msg != 'error') {
		$('#changeorder_' + msg).parent().parent().hide();
	}
}